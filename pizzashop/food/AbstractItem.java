package pizzashop.food;
/**
 * AbstractItem is abstract class by implement OrderItem interface
 * @author Sorawit Sakulkalanuwat
 *
 */
public abstract class AbstractItem implements OrderItem{
	// size of OrderItem
	public static final String[] sizes = { "None", "Small", "Medium", "Large" };
	// size of abstractItem
	protected int size;
	
	/**
	 * constructor method of AbstractItem
	 * @param size of AbstractItem
	 */
	public AbstractItem(int size){
		this.size = size;
	}
	/**
	 * for get price
	 */
	public abstract double getPrice();
	/**
	 * for set size
	 */
	public abstract void setSize(int size);
	/**
	 * for get String
	 */
	public abstract String toString();
}
