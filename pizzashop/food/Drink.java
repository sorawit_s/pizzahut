package pizzashop.food;

/**
 * A drink has a size and flavor.
 * @author Sorawit Sakulkalanuwat
 */
public class Drink extends AbstractItem implements OrderItem{
	// constants related to drink size
	public static final double[] prices = { 0.0, 20.0, 30.0, 40.0 };
	// flavor contain flavor of drink
	private String flavor;

	/**
	 * create a new drink.
	 * 
	 * @param size
	 *            is the size. 1=small, 2=medium, 3=large
	 */
	public Drink(int size) {
		super(size);
	}

	/**
	 * for set size
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/** 
	 *  for get string
	 */
	public String toString() {
		return sizes[size] + " " + (flavor == null ? "" : flavor.toString())
				+ " drink";
	}

	/**
	 * return the price of a drink
	 * 
	 * @see pizzashop.FoodItem#getPrice()
	 */
	public double getPrice() {
		if (size >= 0 && size < prices.length)
			return prices[size];
		return 0.0;
	}
	
	/**
	 * clone for clone drink object
	 */
	public Object clone() {
		Drink clone;

		try {
			clone = (Drink) super.clone();
		} catch (CloneNotSupportedException e) {
			System.err.println("Drink.clone: " + e);
			clone = new Drink(0);
		}
		// superclass already cloned the size and flavor
		return clone;
	}
}
