package pizzashop.food;

/**
 * OrderItem for contain getPrice, setSize, toString method
 * @author Sorawit Sakulkalanuwat
 *
 */
public interface OrderItem {
	/**
	 * for get price
	 */
	public double getPrice();
	/**
	 * for set size
	 * @param size to set
	 */
	public void setSize(int size);
	/**
	 * for get String
	 */
	public String toString();
}
